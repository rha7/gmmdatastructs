package gmmdatastructs_test

import (
	"bitbucket.org/rha7/gmmdatastructs"
	. "github.com/smartystreets/goconvey/convey"
	"log"
	"testing"
)

type TestSinglyLinkedNode struct {
	gmmdatastructs.SinglyLinkedListBaseNode
	Value int
}

func TestSinglyLinkedList(t *testing.T) {
	Convey("Given a SinglyLinkedList and a set of nodes", t, func() {
		nodeOne := &TestSinglyLinkedNode{
			Value: 100,
		}
		nodeTwo := &TestSinglyLinkedNode{
			Value: 200,
		}
		nodeThree := &TestSinglyLinkedNode{
			Value: 300,
		}
		sll := gmmdatastructs.NewSinglyLinkedList()
		Convey("When verifying that SinglyLinkedList", func() {
			Convey("Then it should not be nil, but should be empty", func() {
				So(sll, ShouldNotBeNil)
				So(sll.IndexNode, ShouldBeNil)
				So(sll.LastNode, ShouldBeNil)
			})
		})
		Convey("When verifying them", func() {
			Convey("Then they should implement SinglyLinkedNode", func() {
				So(nodeOne, ShouldImplement, (*gmmdatastructs.SinglyLinkedNode)(nil))
			})
		})
		Convey("When prepending the first node to the SinglyLinkedList", func() {
			sll.Prepend(nodeOne)
			Convey("Then Index and Last node pointers should point to it", func() {
				So(sll.IndexNode, ShouldEqual, nodeOne)
				So(sll.LastNode, ShouldEqual, nodeOne)
			})
			Convey("When prepending the second node to the SinglyLinkedList", func() {
				sll.Prepend(nodeTwo)
				Convey("Then Index and Last node pointers should point accordingly", func() {
					So(sll.IndexNode, ShouldEqual, nodeTwo)
					So(sll.LastNode, ShouldEqual, nodeOne)
				})
			})
			Convey("When appending the second node to the SinglyLinkedList", func() {
				sll.Append(nodeTwo)
				Convey("Then Index and Last node pointers should point accordingly", func() {
					So(sll.IndexNode, ShouldEqual, nodeOne)
					So(sll.LastNode, ShouldEqual, nodeTwo)
				})
			})
		})
		Convey("When appending the first node to the SinglyLinkedList", func() {
			sll.Append(nodeOne)
			Convey("Then Index and Last node pointers should point to it", func() {
				So(sll.IndexNode, ShouldEqual, nodeOne)
				So(sll.LastNode, ShouldEqual, nodeOne)
			})
			Convey("When prepending the second node to the SinglyLinkedList", func() {
				sll.Prepend(nodeTwo)
				Convey("Then Index and Last node pointers should point accordingly", func() {
					So(sll.IndexNode, ShouldEqual, nodeTwo)
					So(sll.LastNode, ShouldEqual, nodeOne)
				})
			})
			Convey("When appending the second node to the SinglyLinkedList", func() {
				sll.Append(nodeTwo)
				Convey("Then Index and Last node pointers should point accordingly", func() {
					So(sll.IndexNode, ShouldEqual, nodeOne)
					So(sll.LastNode, ShouldEqual, nodeTwo)
				})
			})
		})
		Convey("Given a SinglyLinkedList containing a set of SinglyLinkedNodes", func() {
			sll.Append(nodeOne)
			sll.Append(nodeTwo)
			sll.Append(nodeThree)
			Convey("When I traverse them", func() {
				dataIn := make([]int, 0, 3)
				finger := sll.IndexNode.(*TestSinglyLinkedNode)
				for {
					dataIn = append(dataIn, finger.Value)
					if finger.GetNext() == nil {
						break
					}
					finger = finger.GetNext().(*TestSinglyLinkedNode)
				}
				Convey("Then I should get them in the correct order", func() {
					So(dataIn[0], ShouldEqual, 100)
					So(dataIn[1], ShouldEqual, 200)
					So(dataIn[2], ShouldEqual, 300)
				})
			})
		})
		Convey("Given a SinglyLinkedList with a set of SinglyLinkedNodes", func() {
			sll.Append(nodeOne)
			sll.Append(nodeTwo)
			sll.Append(nodeThree)
			Convey("When I remove the one at the head among many", func() {
				if err := sll.Remove(nodeOne); err != nil {
					log.Fatal(err)
				}
				Convey("It should be correctly removed without side effects", func() {
					So(sll.IndexNode, ShouldNotBeNil)
					So(sll.LastNode, ShouldNotBeNil)
					So(sll.IndexNode, ShouldEqual, nodeTwo)
					So(sll.LastNode, ShouldEqual, nodeThree)
					So(nodeOne.GetNext(), ShouldBeNil)
					So(nodeTwo.GetNext(), ShouldEqual, nodeThree)
					So(nodeThree.GetNext(), ShouldBeNil)
				})
			})
			Convey("When I remove the one at the tail among many", func() {
				if err := sll.Remove(nodeThree); err != nil {
					log.Fatal(err)
				}
				Convey("It should be correctly removed without side effects", func() {
					So(sll.IndexNode, ShouldNotBeNil)
					So(sll.LastNode, ShouldNotBeNil)
					So(sll.IndexNode, ShouldEqual, nodeOne)
					So(sll.LastNode, ShouldEqual, nodeTwo)
					So(nodeThree.GetNext(), ShouldBeNil)
					So(nodeOne.GetNext(), ShouldEqual, nodeTwo)
					So(nodeTwo.GetNext(), ShouldBeNil)
				})
			})
			Convey("When I remove one in the middle among many", func() {
				if err := sll.Remove(nodeTwo); err != nil {
					log.Fatal(err)
				}
				Convey("It should be correctly removed without side effects", func() {
					So(sll.IndexNode, ShouldNotBeNil)
					So(sll.LastNode, ShouldNotBeNil)
					So(sll.IndexNode, ShouldEqual, nodeOne)
					So(sll.LastNode, ShouldEqual, nodeThree)
					So(nodeTwo.GetNext(), ShouldBeNil)
					So(nodeOne.GetNext(), ShouldEqual, nodeThree)
					So(nodeThree.GetNext(), ShouldBeNil)
				})
			})
			Convey("When I remove the last one remaining", func() {
				if err := sll.Remove(nodeOne); err != nil {
					log.Fatal(err)
				}
				if err := sll.Remove(nodeTwo); err != nil {
					log.Fatal(err)
				}
				if err := sll.Remove(nodeThree); err != nil {
					log.Fatal(err)
				}
				Convey("Then it should be correctly removed without side effects", func() {
					So(sll.IndexNode, ShouldBeNil)
					So(sll.LastNode, ShouldBeNil)
					So(nodeOne.GetNext(), ShouldBeNil)
					So(nodeTwo.GetNext(), ShouldBeNil)
					So(nodeThree.GetNext(), ShouldBeNil)
				})
			})
			Convey("When I try to remove a nil node", func() {
				err := sll.Remove(nil)
				Convey("Then I should get an error", func() {
					So(err, ShouldNotBeNil)
					So(err.Error(), ShouldContainSubstring, "node <nil> was nil")
				})
			})
			Convey("When I try to remove a nil node from an empty list", func() {
				emptySLL := gmmdatastructs.NewSinglyLinkedList()
				err := emptySLL.Remove(nil)
				Convey("Then I should get an error", func() {
					So(err, ShouldNotBeNil)
					So(err.Error(), ShouldContainSubstring, "was nil")
					So(err.Error(), ShouldContainSubstring, "was not found in list")
				})
			})
			Convey("When I try to remove a non-nil node from an empty list", func() {
				newNode := &TestSinglyLinkedNode{
					Value: 500,
				}
				emptySLL := gmmdatastructs.NewSinglyLinkedList()
				err := emptySLL.Remove(newNode)
				Convey("Then I should get an error", func() {
					So(err, ShouldNotBeNil)
					So(err.Error(), ShouldContainSubstring, "was not found in list")
				})
			})
			Convey("When I try to remove a non-nil node from a list that doesn't contain it", func() {
				newNode := &TestSinglyLinkedNode{
					Value: 500,
				}
				err := sll.Remove(newNode)
				Convey("Then I should get an error", func() {
					So(err, ShouldNotBeNil)
					So(err.Error(), ShouldContainSubstring, "was not found in list")
				})
			})
		})
	})
}
