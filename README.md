# GMM Dat Structures

Honing skills in Go/Golang after a while.

Includes:

* Stack (push, pop only)
* SinglyLinkedList (could act as a queue too, currently has prepend, append and remove, needs pop and dequeue)
* DoublyLinkedList (circular)
