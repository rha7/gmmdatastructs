package gmmdatastructs_test

import (
	"bitbucket.org/rha7/gmmdatastructs"
	. "github.com/smartystreets/goconvey/convey"
	"log"
	"testing"
)

type TestStackNode struct {
	gmmdatastructs.StackBaseNode
	Value int
}

func getTestStackNodeWithValue(stackNode gmmdatastructs.StackNode) (*TestStackNode, int) {
	if stackNode == nil {
		return nil, -1
	}
	testStackNode, ok := stackNode.(*TestStackNode)
	if !ok {
		log.Fatal("unable to assert type *TestStackNode from StackNode")
	}
	return testStackNode, testStackNode.Value
}

func TestStack(t *testing.T) {
	testNodes := struct {
		Node1 gmmdatastructs.StackNode
		Node2 gmmdatastructs.StackNode
		Node3 gmmdatastructs.StackNode
		Node4 gmmdatastructs.StackNode
	}{
		&TestStackNode{
			Value: 100,
		},
		&TestStackNode{
			Value: 200,
		},
		&TestStackNode{
			Value: 300,
		},
		&TestStackNode{
			Value: 400,
		},
	}
	Convey("Given an empty Stack", t, func() {
		stack := gmmdatastructs.NewStack()
		Convey("When I add a node to it", func() {
			stack.Push(testNodes.Node1)
			Convey("Then it should be added correctly", func() {
				So(stack.IndexNode, ShouldNotBeNil)
				So(stack.IndexNode, ShouldEqual, testNodes.Node1)
				So(stack.IndexNode.GetNext(), ShouldBeNil)
			})
		})
		Convey("When I add a second node to it", func() {
			stack.Push(testNodes.Node1)
			stack.Push(testNodes.Node2)
			Convey("Then it should be added correctly", func() {
				So(stack.IndexNode, ShouldNotBeNil)
				So(stack.IndexNode, ShouldEqual, testNodes.Node2)
				So(stack.IndexNode.GetNext(), ShouldEqual, testNodes.Node1)
				So(stack.IndexNode.GetNext().GetNext(), ShouldBeNil)
			})
		})
		Convey("When I pop a node from it", func() {
			node := stack.Pop()
			Convey("Then I should get a nil response", func() {
				So(node, ShouldBeNil)
				So(stack.IndexNode, ShouldBeNil)
			})
		})
	})
	Convey("Given a Stack with only one node in it", t, func() {
		stack := gmmdatastructs.NewStack()
		stack.Push(testNodes.Node1)
		Convey("When I add a second node to it", func() {
			stack.Push(testNodes.Node2)
			Convey("Then it should be added correctly", func() {
				So(stack.IndexNode, ShouldNotBeNil)
				So(stack.IndexNode, ShouldEqual, testNodes.Node2)
				So(stack.IndexNode.GetNext(), ShouldEqual, testNodes.Node1)
				So(stack.IndexNode.GetNext().GetNext(), ShouldBeNil)
			})
		})
		Convey("When I add a third node to it", func() {
			stack.Push(testNodes.Node2)
			stack.Push(testNodes.Node3)
			Convey("Then it should be added correctly", func() {
				So(stack.IndexNode, ShouldNotBeNil)
				So(stack.IndexNode, ShouldEqual, testNodes.Node3)
				So(stack.IndexNode.GetNext(), ShouldEqual, testNodes.Node2)
				So(stack.IndexNode.GetNext().GetNext(), ShouldEqual, testNodes.Node1)
				So(stack.IndexNode.GetNext().GetNext().GetNext(), ShouldBeNil)
			})
		})
		Convey("When I pop a node from it", func() {
			node, value := getTestStackNodeWithValue(stack.Pop())
			Convey("Then I should get the existing node", func() {
				So(node, ShouldEqual, testNodes.Node1)
				So(node.GetNext(), ShouldBeNil)
				So(value, ShouldEqual, 100)
				So(stack.IndexNode, ShouldBeNil)
			})
		})
		Convey("When I pop one extra node than stack had in it", func() {
			nodeA, valueA := getTestStackNodeWithValue(stack.Pop())
			nodeB, _ := getTestStackNodeWithValue(stack.Pop())
			Convey("Then I should get a nil node", func() {
				So(stack.IndexNode, ShouldBeNil)
				So(nodeA, ShouldEqual, testNodes.Node1)
				So(nodeA.GetNext(), ShouldBeNil)
				So(valueA, ShouldEqual, 100)
				So(nodeB, ShouldBeNil)
			})
		})
	})
	Convey("Given a Stack with two (more than one) nodes in it", t, func() {
		stack := gmmdatastructs.NewStack()
		stack.Push(testNodes.Node1)
		stack.Push(testNodes.Node2)
		Convey("When I add a third node to it", func() {
			stack.Push(testNodes.Node3)
			Convey("Then it should be added correctly", func() {
				So(stack.IndexNode, ShouldEqual, testNodes.Node3)
				So(stack.IndexNode.GetNext(), ShouldEqual, testNodes.Node2)
				So(stack.IndexNode.GetNext().GetNext(), ShouldEqual, testNodes.Node1)
				So(stack.IndexNode.GetNext().GetNext().GetNext(), ShouldBeNil)
			})
		})
		Convey("When I add a fourth node to it", func() {
			stack.Push(testNodes.Node3)
			stack.Push(testNodes.Node4)
			Convey("Then it should be added correctly", func() {
				So(stack.IndexNode, ShouldEqual, testNodes.Node4)
				So(stack.IndexNode.GetNext(), ShouldEqual, testNodes.Node3)
				So(stack.IndexNode.GetNext().GetNext(), ShouldEqual, testNodes.Node2)
				So(stack.IndexNode.GetNext().GetNext().GetNext(), ShouldEqual, testNodes.Node1)
				So(stack.IndexNode.GetNext().GetNext().GetNext().GetNext(), ShouldBeNil)
			})
		})
		Convey("When I pop a node from it", func() {
			testNode, testValue := getTestStackNodeWithValue(stack.Pop())
			Convey("Then I should get the existing node", func() {
				So(stack.IndexNode, ShouldEqual, testNodes.Node1)
				So(stack.IndexNode.GetNext(), ShouldBeNil)
				So(testNode, ShouldNotBeNil)
				So(testNode, ShouldEqual, testNodes.Node2)
				So(testNode.GetNext(), ShouldBeNil)
				So(testValue, ShouldEqual, 200)
			})
		})
		Convey("When I pop all nodes from it", func() {
			nodeA, valueA := getTestStackNodeWithValue(stack.Pop())
			nodeB, valueB := getTestStackNodeWithValue(stack.Pop())
			Convey("Then I should get all nodes correctly", func() {
				So(stack.IndexNode, ShouldBeNil)
				So(nodeA, ShouldEqual, testNodes.Node2)
				So(nodeA.GetNext(), ShouldBeNil)
				So(valueA, ShouldEqual, 200)
				So(nodeB, ShouldEqual, testNodes.Node1)
				So(nodeA.GetNext(), ShouldBeNil)
				So(valueB, ShouldEqual, 100)
			})
		})
		Convey("When I pop one extra node than stack had in it", func() {
			nodeA, valueA := getTestStackNodeWithValue(stack.Pop())
			nodeB, valueB := getTestStackNodeWithValue(stack.Pop())
			nodeC, _ := getTestStackNodeWithValue(stack.Pop())
			Convey("Then I should get nil", func() {
				So(stack.IndexNode, ShouldBeNil)
				So(nodeA, ShouldNotBeNil)
				So(nodeA, ShouldEqual, testNodes.Node2)
				So(valueA, ShouldEqual, 200)
				So(nodeA.GetNext(), ShouldBeNil)
				So(nodeB, ShouldNotBeNil)
				So(nodeB, ShouldEqual, testNodes.Node1)
				So(valueB, ShouldEqual, 100)
				So(nodeB.GetNext(), ShouldBeNil)
				So(nodeC, ShouldBeNil)
			})
		})
	})
}
