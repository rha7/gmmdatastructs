package gmmdatastructs

import (
    "fmt"
)

type SinglyLinkedNode interface {
    SetNext(SinglyLinkedNode)
    GetNext() SinglyLinkedNode
}

type SinglyLinkedListBaseNode struct {
    Next SinglyLinkedNode
}

func (thisNode *SinglyLinkedListBaseNode) SetNext(node SinglyLinkedNode) {
    thisNode.Next = node
}

func (thisNode *SinglyLinkedListBaseNode) GetNext() SinglyLinkedNode {
    return thisNode.Next
}

var _ SinglyLinkedNode = (*SinglyLinkedListBaseNode)(nil)

type SinglyLinkedList struct {
    IndexNode SinglyLinkedNode
    LastNode  SinglyLinkedNode
}

func NewSinglyLinkedList() *SinglyLinkedList {
    return &SinglyLinkedList{
        IndexNode: nil,
        LastNode:  nil,
    }
}

func (thisList *SinglyLinkedList) Prepend(node SinglyLinkedNode) {
    if thisList.IndexNode == nil && thisList.LastNode == nil {
        thisList.IndexNode = node
        thisList.LastNode = node
        return
    }
    node.SetNext(thisList.IndexNode)
    thisList.IndexNode = node
}

func (thisList *SinglyLinkedList) Append(node SinglyLinkedNode) {
    if thisList.IndexNode == nil && thisList.LastNode == nil {
        thisList.IndexNode = node
        thisList.LastNode = node
        return
    }
    thisList.LastNode.SetNext(node)
    node.SetNext(nil)
    thisList.LastNode = node
}

func (thisList *SinglyLinkedList) Remove(node SinglyLinkedNode) error {
    for prev, curr := (SinglyLinkedNode)(nil), thisList.IndexNode; node != nil && curr != nil; prev, curr = curr, curr.GetNext() {
        if curr == node {
            if thisList.IndexNode == curr {
                thisList.IndexNode = curr.GetNext()
            }
            if thisList.LastNode == curr {
                thisList.LastNode = prev
            }
            if prev != nil {
                prev.SetNext(curr.GetNext())
            }
            node.SetNext(nil)
            return nil
        }
    }
    return fmt.Errorf("node %#v was nil or was not found in list", node)
}
