package gmmdatastructs

import (
    "fmt"
)

type Node interface {
    SetPrev(Node)
    SetNext(Node)
    GetPrev() Node
    GetNext() Node
}

type DoublyLinkedListBaseNode struct {
    Prev Node
    Next Node
    Value int
}

func (thisNode *DoublyLinkedListBaseNode) SetPrev(node Node) {
    thisNode.Prev = node
}

func (thisNode *DoublyLinkedListBaseNode) SetNext(node Node) {
    thisNode.Next = node
}

func (thisNode *DoublyLinkedListBaseNode) GetPrev() Node {
    return thisNode.Prev
}

func (thisNode *DoublyLinkedListBaseNode) GetNext() Node {
    return thisNode.Next
}

var _ Node = (*DoublyLinkedListBaseNode)(nil)

type DoublyLinkedList struct {
    IndexNode Node
}

func NewDoublyLinkedList() *DoublyLinkedList {
    return &DoublyLinkedList{
        IndexNode: nil,
    }
}

func (dll *DoublyLinkedList) Insert(node Node) (Node, error) {
    if dll == nil {
        return nil, fmt.Errorf("non existant linked list")
    }
    if dll.IndexNode == nil {
        node.SetPrev(node)
        node.SetNext(node)
        dll.IndexNode = node
        return node, nil
    }
    currentIndexNode := dll.IndexNode
    currentNextNode := dll.IndexNode.GetNext()

    currentIndexNode.SetNext(node)
    currentNextNode.SetPrev(node)
    node.SetPrev(currentIndexNode)
    node.SetNext(currentNextNode)
    dll.IndexNode = node
    return dll.IndexNode, nil
}

func (dll *DoublyLinkedList) Remove(node Node) (Node, error) {
    if node == nil || node.GetPrev() == nil || node.GetNext() == nil {
        return nil, fmt.Errorf("node not found or invalid Next or previous links")
    }
    if dll.IndexNode == node { // if removing IndexNode
        dll.IndexNode = node.GetNext() // move IndexNode to next node
        if dll.IndexNode.GetNext() == dll.IndexNode { // if pointing to itself, we're removing the last one,
            dll.IndexNode = nil // so, point IndexNode to nil, and remove the node below.
        }
    }
    node.GetPrev().SetNext(node.GetNext())
    node.GetNext().SetPrev(node.GetPrev())
    node.SetPrev(nil)
    node.SetNext(nil)
    return node, nil
}

func (dll *DoublyLinkedList) Next() (Node, error) {
    if dll.IndexNode == nil || dll.IndexNode.GetPrev() == nil || dll.IndexNode.GetNext() == nil {
        return nil, fmt.Errorf("doubly linked list is empty or previous/next links are missing")
    }
    dll.IndexNode = dll.IndexNode.GetNext()
    return dll.IndexNode, nil
}

func (dll *DoublyLinkedList) Prev() (Node, error) {
    if dll.IndexNode == nil || dll.IndexNode.GetPrev() == nil || dll.IndexNode.GetNext() == nil {
        return nil, fmt.Errorf("doubly linked list is empty or previous/next links are missing")
    }
    dll.IndexNode = dll.IndexNode.GetPrev()
    return dll.IndexNode, nil
}
