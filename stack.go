package gmmdatastructs

type StackNode interface {
    GetNext() StackNode
    SetNext(node StackNode)
}

type StackBaseNode struct {
    Next StackNode
}

var _ StackNode = (*StackBaseNode)(nil)

func (thisNode *StackBaseNode) GetNext() StackNode {
    return thisNode.Next
}

func (thisNode *StackBaseNode) SetNext(node StackNode) {
    thisNode.Next = node
}

type Stack struct {
    IndexNode StackNode
}

func NewStack() *Stack {
    return &Stack{
        IndexNode: nil,
    }
}

func (thisStack *Stack) Push(node StackNode) {
    node.SetNext(thisStack.IndexNode)
    thisStack.IndexNode = node
}

func (thisStack *Stack) Pop() StackNode {
    if thisStack.IndexNode == nil {
        return nil
    }
    next := thisStack.IndexNode.GetNext()
    node := thisStack.IndexNode
    node.SetNext(nil)
    thisStack.IndexNode = next
    return node
}
