package gmmdatastructs_test

import (
	"bitbucket.org/rha7/gmmdatastructs"
	"fmt"
	"testing"
)

type TestDoublyLinkedNode struct {
	gmmdatastructs.DoublyLinkedListBaseNode
	Value int
}

var _ gmmdatastructs.Node = (*TestDoublyLinkedNode)(nil)

func TestDoublyLinkedList(t *testing.T) {
	var err error
	var dll *gmmdatastructs.DoublyLinkedList

	// Invalid linked list
	_, err = dll.Insert(&TestDoublyLinkedNode{Value: 5})
	if err == nil {
		t.Fatal("nil linked list did not cause errors")
	}

	// Valid linked list
	dll = gmmdatastructs.NewDoublyLinkedList()
	if dll == nil {
		t.Fatal("error creating a new DoublyLinkedList, got nil")
	}

	verifyNode := func(t *testing.T, node gmmdatastructs.Node, err error, value int, prevNode gmmdatastructs.Node, nextNode gmmdatastructs.Node) error {
		testNode, ok := node.(*TestDoublyLinkedNode)
		if !ok {
			t.Fatal("unable to cast TestDoublyLinkedNode to Node")
		}
		if err != nil {
			return fmt.Errorf("error inserting new value: %s", err.Error())
		}
		if node == nil {
			return fmt.Errorf("node pointer is nil: %#v", testNode)
		}
		if node.GetPrev() != prevNode {
			return fmt.Errorf("node %#v previous node does not match: expected %#v, got %#v", node, prevNode, testNode.Prev)
		}
		if node.GetNext() != nextNode {
			return fmt.Errorf("node %#v next node does not match: expected %#v, got %#v", node, nextNode, testNode.Next)
		}
		if testNode.Value != value {
			return fmt.Errorf("node %#v extracted value %#v does not match expected value %#v", node, testNode.Value, value)
		}
		return nil
	}

	var verificationError error
	node1 := &TestDoublyLinkedNode{Value: 1001}
	_, err = dll.Insert(node1)
	verificationError = verifyNode(t, node1, err, 1001, node1, node1)
	if verificationError != nil {
		t.Error(verificationError)
	}

	node2 := &TestDoublyLinkedNode{Value: 1002}
	_, err = dll.Insert(node2)
	verificationError = verifyNode(t, node2, err, 1002, node1, node1)
	if verificationError != nil {
		t.Error(verificationError)
	}

	node3 := &TestDoublyLinkedNode{Value: 1003}
	_, err = dll.Insert(node3)
	verificationError = verifyNode(t, node3, err, 1003, node2, node1)
	if verificationError != nil {
		t.Error(verificationError)
	}

	node4 := &TestDoublyLinkedNode{Value: 1004}
	_, err = dll.Insert(node4)
	verificationError = verifyNode(t, node4, err, 1004, node3, node1)
	if verificationError != nil {
		t.Error(verificationError)
	}

	node5 := &TestDoublyLinkedNode{Value: 1005}
	_, err = dll.Insert(node5)
	verificationError = verifyNode(t, node5, err, 1005, node4, node1)
	if verificationError != nil {
		t.Error(verificationError)
	}

	verifyNodeIntValue := func(t *testing.T, node gmmdatastructs.Node, err error, expectedValue int) {
		testNode, ok := node.(*TestDoublyLinkedNode)
		if !ok {
			t.Fatal("unable to cast Node to TestDoublyLinkedNode")
		}
		if err != nil {
			t.Error(err)
		}
		if testNode.Value != expectedValue {
			t.Errorf("node %#v error on value, expected %#v, got %#v", node, expectedValue, testNode.Value)
		}
	}

	var node gmmdatastructs.Node

	verifyNodeIntValue(t, dll.IndexNode, nil, 1005)

	node, err = dll.Next()
	verifyNodeIntValue(t, node, err, 1001)
	node, err = dll.Next()
	verifyNodeIntValue(t, node, err, 1002)
	node, err = dll.Next()
	verifyNodeIntValue(t, node, err, 1003)
	node, err = dll.Next()
	verifyNodeIntValue(t, node, err, 1004)
	node, err = dll.Next()
	verifyNodeIntValue(t, node, err, 1005)

	node, err = dll.Prev()
	verifyNodeIntValue(t, node, err, 1004)
	node, err = dll.Prev()
	verifyNodeIntValue(t, node, err, 1003)
	node, err = dll.Prev()
	verifyNodeIntValue(t, node, err, 1002)
	node, err = dll.Prev()
	verifyNodeIntValue(t, node, err, 1001)
	node, err = dll.Prev()
	verifyNodeIntValue(t, node, err, 1005)

	node, err = dll.Remove(node1)
	verificationError = verifyNode(t, node, err, 1001, nil, nil)
	if verificationError != nil {
		t.Error(verificationError)
	}
	verificationError = verifyNode(t, node5, nil, 1005, node4, node2)
	if verificationError != nil {
		t.Error(verificationError)
	}
	verificationError = verifyNode(t, node2, nil, 1002, node5, node3)
	if verificationError != nil {
		t.Error(verificationError)
	}

	node, err = dll.Remove(node2)
	verificationError = verifyNode(t, node, err, 1002, nil, nil)
	if verificationError != nil {
		t.Error(verificationError)
	}
	verificationError = verifyNode(t, node5, nil, 1005, node4, node3)
	if verificationError != nil {
		t.Error(verificationError)
	}
	verificationError = verifyNode(t, node3, nil, 1003, node5, node4)
	if verificationError != nil {
		t.Error(verificationError)
	}

	node, err = dll.Remove(node3)
	verificationError = verifyNode(t, node, err, 1003, nil, nil)
	if verificationError != nil {
		t.Error(verificationError)
	}
	verificationError = verifyNode(t, node5, nil, 1005, node4, node4)
	if verificationError != nil {
		t.Error(verificationError)
	}
	verificationError = verifyNode(t, node4, nil, 1004, node5, node5)
	if verificationError != nil {
		t.Error(verificationError)
	}

	node, err = dll.Remove(node4)
	verificationError = verifyNode(t, node, err, 1004, nil, nil)
	if verificationError != nil {
		t.Error(verificationError)
	}
	verificationError = verifyNode(t, node5, nil, 1005, node5, node5)
	if verificationError != nil {
		t.Error(verificationError)
	}

	node, err = dll.Remove(node5)
	verificationError = verifyNode(t, node, err, 1005, nil, nil)
	if verificationError != nil {
		t.Error(verificationError)
	}

	if dll.IndexNode != nil {
		t.Errorf("current IndexNode should be nil, but got %#v", dll.IndexNode)
	}

	node, err = dll.Next()
	if err == nil {
		t.Error("Should have gotten an error when Next()'ing from nil pointer")
	}
	node, err = dll.Prev()
	if err == nil {
		t.Error("Should have gotten an error when Prev()'ing from nil pointer")
	}
	node, err = dll.Remove(nil)
	if err == nil {
		t.Error("Should have gotten an error when Remove()'ing from nil pointer")
	}
}
